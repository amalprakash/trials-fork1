package com.trial;

//import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;


public class SeleniumTrial {

    public WebDriver driver;
    public List<String> searchResults = new ArrayList<String>();

    @BeforeTest
        public void openChrome()
    {
        System.setProperty("webdriver.chrome.driver","C:\\Selenium\\chromedriver.exe");
        ChromeOptions startMaximized = new ChromeOptions();
        startMaximized.addArguments("start-maximized");
        driver = new ChromeDriver(startMaximized);
        driver.get("https://www.google.com");
    }

    @BeforeMethod
    public void resetSearchResults()
    {
        driver.get("http://www.google.com");
    }


    @Test
    public void enterQuery1()
    {
        driver.findElement(By.name("q")).sendKeys("top 10 java IDEs");
        driver.findElement(By.name("q")).submit();
        searchResults.add(driver.findElement(By.xpath("//*[@id='rso']/div/div/div[1]/div/div/h3/a")).getText().toString());
    }

    @Test
    public void enterQuery2()
    {
        driver.findElement(By.name("q")).sendKeys("top 10 C++ IDEs");
        driver.findElement(By.name("q")).submit();
        searchResults.add(driver.findElement(By.xpath("//*[@id='rso']/div/div/div[1]/div/div/h3/a")).getText().toString());

    }

    @Test
    public void enterQuery3()
    {
        driver.findElement(By.name("q")).sendKeys("top 10 python IDEs");
        driver.findElement(By.name("q")).submit();
        searchResults.add(driver.findElement(By.xpath("//*[@id='rso']/div/div/div[1]/div/div/h3/a")).getText().toString());

    }

    @AfterTest
    public void closeBrowser()
    {
        driver.quit();
       searchResults.forEach(searchResults -> System.out.println("Result-->"+searchResults));
    }


}
